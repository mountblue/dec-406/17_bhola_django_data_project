from django.core.management.base import BaseCommand, CommandError
from ipl.models import Matches, Deliveries
from django.db import connection, transaction
import os
import ipl
import time
import csv
from itertools import islice
from django.conf import settings


class Command(BaseCommand):
    help = 'Load CSV data into tables!'

    def add_arguments(self, parser):
        parser.add_argument('method', nargs='?', type=int, default=2,
                            help="Loading Type; Option 2 is default; "
                                 "1=Load Data Infile (only for Mysql DB), "
                                 "2=Bluk create with Transaction Atomic")

    def handle(self, *args, **options):
        if options['method'] in [1, 2]:
            start_time = time.time()
            # getting app directory
            pth = os.path.dirname(ipl.__file__)
            file_location_matches =\
                pth + "/static/ipl/res/csv/matches.csv"
            file_location_deliveries =\
                pth + "/static/ipl/res/csv/deliveries.csv"

            my_cursor = connection.cursor()

            if self.check_table_exists(my_cursor):
                pass
            else:
                self.stdout.write(
                    self.style.ERROR('Tables doen not exists!'))

            self.reset_tables(my_cursor)

            if options['method'] == 2:
                self.bulk_create_transaction_method(my_cursor,
                                                    file_location_matches,
                                                    file_location_deliveries)

            if options['method'] == 1:
                if self.check_database_type():
                    self.load_data_infile_method(my_cursor,
                                                 file_location_matches,
                                                 file_location_deliveries)
                else:
                    self.stdout.write(
                        self.style.ERROR('You are not using MySQL database! Try others method.'))

            end_time = time.time() - start_time
            self.stdout.write('Executed in ' + str(end_time) + 'sec...')
        else:
            self.stdout.write(self.style.ERROR('Invalid input!'))
            return

    def check_table_exists(self, my_cursor):
        all_tables = connection.introspection.get_table_list(my_cursor)
        all_tables_name = [table[0] for table in all_tables]
        my_tables_name = ['ipl_matches', 'ipl_deliveries']
        if (my_tables_name[0] in all_tables_name) and\
           (my_tables_name[1] in all_tables_name):
            return True
        return False

    def reset_tables(self, my_cursor):
        # if there is data in table first reset it
        sql_truncate_deliveries = "TRUNCATE ipl_deliveries"
        my_cursor.execute(sql_truncate_deliveries)
        sql_delete_data_matches = "DELETE from ipl_matches where id > 0;"
        my_cursor.execute(sql_delete_data_matches)

    def check_database_type(self):
        database_engine = settings.DATABASES['default']['ENGINE']
        if database_engine == "django.db.backends.mysql":
            return True
        return False

    def load_data_infile_method(self, my_cursor, file_location_matches,
                                file_location_deliveries):
        # Load data infile method
        self.stdout.write(
            self.style.WARNING('NOTE: Using Load Data Infile Query!'))

        sql_query_match =\
            "LOAD DATA LOCAL INFILE '"+file_location_matches+"' "\
            "INTO TABLE ipl_matches FIELDS TERMINATED BY ',' "\
            "ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS"

        self.stdout.write('Loading \"matches.csv\"...')
        my_cursor.execute(sql_query_match)

        self.stdout.write('Loaded \"deliveries.csv\"...')

        sql_query_delivery =\
            "LOAD DATA LOCAL INFILE '"+file_location_deliveries+"' "\
            "INTO TABLE ipl_deliveries FIELDS TERMINATED BY ',' "\
            "ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS "\
            "(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10"\
            ",@col11,@col12,@col13,@col14,@col15,@col16,@col17,@col18,@col19"\
            ",@col20,@col21) set match_id_id=@col1,inning=@col2,"\
            "batting_team=@col3,bowling_team=@col4,overs=@col5,ball=@col6,"\
            "batsman=@col7,non_striker=@col8,bowler=@col9,is_super_over="\
            "@col10,wide_runs=@col11,bye_runs=@col12,legbye_runs=@col13,"\
            "noball_runs=@col14,penalty_runs=@col15,batsman_runs=@col16,"\
            "extra_runs=@col17,total_runs=@col18,player_dismissed=@col19,"\
            "dismissal_kind=@col20,fielder=@col21"

        self.stdout.write('Loading \"deliveries.csv\"...')
        my_cursor.execute(sql_query_delivery)

        self.stdout.write('Loaded \"deliveries.csv\"...')

        self.stdout.write(self.style.SUCCESS('Data successfully loaded!'))

    def bulk_create_transaction_method(self, my_cursor, file_location_matches,
                                       file_location_deliveries):

        # using bluk create method with transaction atomic
        self.stdout.write(
            self.style.WARNING('NOTE: Using bluk create method '
                               'with transaction atomic!'))

        self.stdout.write('Loading \"matches.csv\"...')
        with open(file_location_matches) as matches_csv_file:
            matches_reader = csv.DictReader(matches_csv_file, delimiter=',')
            matches_obj = []
            for match in matches_reader:
                obj =\
                    Matches(id=match['id'], season=match['season'],
                            city=match['city'], date=match['date'],
                            team1=match['team1'], team2=match['team2'],
                            toss_winner=match['toss_winner'],
                            toss_decision=match['toss_decision'],
                            result=match['result'],
                            dl_applied=match['dl_applied'],
                            winner=match['winner'],
                            win_by_runs=match['win_by_runs'],
                            win_by_wickets=match['win_by_wickets'],
                            player_of_match=match['player_of_match'],
                            venue=match['venue'], umpire1=match['umpire1'],
                            umpire2=match['umpire2'], umpire3=match['umpire3'])
                matches_obj.append(obj)

            self.my_batch_load(Matches, matches_obj)
            self.stdout.write('Loaded \"matches.csv\"...')

        self.stdout.write('Loading \"deliveries.csv\"...')
        with open(file_location_deliveries) as deliveries_csv_file:
            deliveries_reader =\
                csv.DictReader(deliveries_csv_file, delimiter=',')
            deliveries_obj = []
            for delivery in deliveries_reader:
                obj =\
                    Deliveries(match_id_id=delivery['match_id'],
                               inning=delivery['inning'],
                               batting_team=delivery['batting_team'],
                               bowling_team=delivery['bowling_team'],
                               overs=delivery['over'], ball=delivery['ball'],
                               batsman=delivery['batsman'],
                               non_striker=delivery['non_striker'],
                               bowler=delivery['bowler'],
                               is_super_over=delivery['is_super_over'],
                               wide_runs=delivery['wide_runs'],
                               bye_runs=delivery['bye_runs'],
                               legbye_runs=delivery['legbye_runs'],
                               noball_runs=delivery['noball_runs'],
                               penalty_runs=delivery['penalty_runs'],
                               batsman_runs=delivery['batsman_runs'],
                               extra_runs=delivery['extra_runs'],
                               total_runs=delivery['total_runs'],
                               player_dismissed=delivery['player_dismissed'],
                               dismissal_kind=delivery['dismissal_kind'],
                               fielder=delivery['fielder'])
                deliveries_obj.append(obj)

            self.my_batch_load(Deliveries, deliveries_obj)
            self.stdout.write('Loaded \"deliveries.csv\"...')

            self.stdout.write(self.style.SUCCESS('Data successfully loaded!'))

    def my_batch_load(self, obj, objs):
        batch_size = 2000
        batch_start = 0
        batch_end = batch_size
        while True:
            batch = list(islice(objs, batch_start, batch_end))
            batch_start = batch_end
            batch_end = batch_end + batch_size
            if not batch:
                break
            with transaction.atomic():
                obj.objects.bulk_create(batch, batch_size)
