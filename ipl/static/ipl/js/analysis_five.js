// Function to plot graph
function plotGraph(jsonData){

    console.log("Plotting graph...");
    batToss = jsonData['bat'];
    fieldToss = jsonData['field'];

    totalToss = batToss + fieldToss;

    batPercentage = (batToss/totalToss)*100;
    fieldPercentage = (fieldToss/totalToss)*100;

    console.log(batPercentage);
    console.log(fieldPercentage);

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Wins Toss percentage'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Toss',
            colorByPoint: true,
            data: [{
                name: 'Bat',
                y: batPercentage
            }, {
                name: 'Bowl',
                y: fieldPercentage
            }]
        }]
    });

}


// function to show error when json not found
function showError(){
    document.getElementById('container').innerHTML = "<p>Something went wrong!<p>";
}


// function to make Http Object
function makeHttpObject() {
    try {return new XMLHttpRequest();}
    catch (error) {}
    try {return new ActiveXObject("Msxml2.XMLHTTP");}
    catch (error) {}
    try {return new ActiveXObject("Microsoft.XMLHTTP");}
    catch (error) {}

    throw new Error("Could not create HTTP request object.");
}


// main method
function execute(){
    var request = makeHttpObject();
    request.open("GET", jsonUrl, true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState == 4){
            console.log("Success :");
            // console.log(JSON.parse(request.responseText));
            plotGraph(JSON.parse(request.responseText));
        }else{
            console.log("Error!");
            showError();
        }
    };
}


// calling main method
execute();