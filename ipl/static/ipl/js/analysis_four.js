// Function to plot graph
function plotGraph(jsonData){

    console.log("Plotting graph...");

    let dataToPass = jsonData.map(bowlerEconomical => [bowlerEconomical['bowler'], bowlerEconomical['economical']])
    console.log(dataToPass);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 5 Economical bowlers'
        },
        subtitle: {
            text: 'From 2008 to 2017 in IPL top 5 economical bowlers'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economical'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Economical: <b>{point.y:.2f}</b>'
        },
        series: [{
            name: 'Bowlers',
            data: dataToPass,
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.2f}',
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


// function to show error when json not found
function showError(){
    document.getElementById('container').innerHTML = "<p>Something went wrong!<p>";
}


// function to make Http Object
function makeHttpObject() {
    try {return new XMLHttpRequest();}
    catch (error) {}
    try {return new ActiveXObject("Msxml2.XMLHTTP");}
    catch (error) {}
    try {return new ActiveXObject("Microsoft.XMLHTTP");}
    catch (error) {}

    throw new Error("Could not create HTTP request object.");
}


// main method
function execute(){
    var request = makeHttpObject();
    request.open("GET", jsonUrl, true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState == 4){
            console.log("Success :");
            // console.log(JSON.parse(request.responseText));
            plotGraph(JSON.parse(request.responseText));
        }else{
            console.log("Error!");
            showError();
        }
    };
}


// calling main method
execute();