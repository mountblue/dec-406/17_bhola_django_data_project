// Function to plot graph
function plotGraph(jsonData){

    console.log("Plotting graph...");

    let dataToPass = jsonData.map(match => [match['season'], match['number_of_matches']])
    console.log(dataToPass);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of matches played per year'
        },
        subtitle: {
            text: 'From 2008 to 2017 in IPL'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: 0,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Number of matches: <b>{point.y:.0f}</b>'
        },
        series: [{
            name: 'Matches',
            data: dataToPass,
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.0f}',
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


// function to show error when json not found
function showError(){
    document.getElementById('container').innerHTML = "<p>Something went wrong!<p>";
}


// function to make Http Object
function makeHttpObject() {
    try {return new XMLHttpRequest();}
    catch (error) {}
    try {return new ActiveXObject("Msxml2.XMLHTTP");}
    catch (error) {}
    try {return new ActiveXObject("Microsoft.XMLHTTP");}
    catch (error) {}

    throw new Error("Could not create HTTP request object.");
}


// main method
function execute(){
    var request = makeHttpObject();
    request.open("GET", jsonUrl, true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState == 4){
            console.log("Success :");
            // console.log(JSON.parse(request.responseText));
            plotGraph(JSON.parse(request.responseText));
        }else{
            console.log("Error!");
            showError();
        }
    };
}


// calling main method
execute();