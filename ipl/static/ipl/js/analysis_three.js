// Function to plot graph
function plotGraph(jsonData){

    console.log("Plotting graph...");

    let dataToPass = jsonData.map(bowlerExtraRuns => [bowlerExtraRuns['bowling_team'], bowlerExtraRuns['sum_extra_runs']])
    console.log(dataToPass);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Bowling team extra runs in 2016'
        },
        subtitle: {
            text: 'Extra runs in 2016 according to bowler team'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra runs'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Extra runs: <b>{point.y:.0f}</b>'
        },
        series: [{
            name: 'Bowling Team Extra Runs',
            data: dataToPass,
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.0f}',
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


// function to show error when json not found
function showError(){
    document.getElementById('container').innerHTML = "<p>Something went wrong!<p>";
}


// function to make Http Object
function makeHttpObject() {
    try {return new XMLHttpRequest();}
    catch (error) {}
    try {return new ActiveXObject("Msxml2.XMLHTTP");}
    catch (error) {}
    try {return new ActiveXObject("Microsoft.XMLHTTP");}
    catch (error) {}

    throw new Error("Could not create HTTP request object.");
}


// main method
function execute(){
    var request = makeHttpObject();
    request.open("GET", jsonUrl, true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState == 4){
            console.log("Success :");
            // console.log(JSON.parse(request.responseText));
            plotGraph(JSON.parse(request.responseText));
        }else{
            console.log("Error!");
            showError();
        }
    };
}


// calling main method
execute();