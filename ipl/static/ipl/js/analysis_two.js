// Function to plot graph
function plotGraph(jsonData){

    console.log("Plotting graph...");

    // getting array of years
    years = [];
    yearsDataInit = [];
    for(let year=2008; year<=2017;year++){
        years.push(year);
        yearsDataInit.push('');
    }

    teams = [];
    let seriesToPass = [];
    jsonData.forEach(element => {
        teamIndex = teams.indexOf(element['winner']);
        yearIndex = years.indexOf(element['season']);
        if(teamIndex != -1){
            seriesToPass[teamIndex]['data'][yearIndex] = element['number_wins'];
        }else{
            seriesToPass.push({name: element['winner'], data: yearsDataInit.slice(0)});
            teams.push(element['winner']);
            teamIndex = teams.indexOf(element['winner']);
            seriesToPass[teamIndex]['data'][yearIndex] = element['number_wins'];
        }
    });

    console.log(seriesToPass);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches won by teams each year in IPL'
        },
        xAxis: {
            // years
            categories: years
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches won'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            x: 0,
            verticalAlign: 'bottom',
            y: 13,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>Year: {point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: seriesToPass
    });

}


// function to show error when json not found
function showError(){
    document.getElementById('container').innerHTML = "<p>Something went wrong!<p>";
}


// function to make Http Object
function makeHttpObject() {
    try {return new XMLHttpRequest();}
    catch (error) {}
    try {return new ActiveXObject("Msxml2.XMLHTTP");}
    catch (error) {}
    try {return new ActiveXObject("Microsoft.XMLHTTP");}
    catch (error) {}

    throw new Error("Could not create HTTP request object.");
}


// main method
function execute(){
    var request = makeHttpObject();
    request.open("GET", jsonUrl, true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState == 4){
            console.log("Success :");
            // console.log(JSON.parse(request.responseText));
            plotGraph(JSON.parse(request.responseText));
        }else{
            console.log("Error!");
            showError();
        }
    };
}


// calling main method
execute();