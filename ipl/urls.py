"""django_data_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    # path(url, view, name),
    path('', views.home, name='ipl-home'),
    path('about/', views.about, name='ipl-about'),
    path('api/analysis/one/', views.api_analysis_one, name='api-analysis-one'),
    path('api/analysis/two/', views.api_analysis_two, name='api-analysis-two'),
    path('api/analysis/three/', views.api_analysis_three,
         name='api-analysis-three'),
    path('api/analysis/four/', views.api_analysis_four,
         name='api-analysis-four'),
    path('api/analysis/five/', views.api_analysis_five,
         name='api-analysis-five'),
    path('analysis/one/', views.analysis_one, name='analysis-one'),
    path('analysis/two/', views.analysis_two, name='analysis-two'),
    path('analysis/three/', views.analysis_three, name='analysis-three'),
    path('analysis/four/', views.analysis_four, name='analysis-four'),
    path('analysis/five/', views.analysis_five, name='analysis-five'),
]
