from django.shortcuts import render
from .models import Matches, Deliveries
from django.db.models import Count, Sum, FloatField, Q, F
from django.db.models.functions import Cast
from django.http import JsonResponse
from django.conf import settings
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL')


# home view
def home(request):
    return render(request, 'ipl/home.html')


# about view
def about(request):
    context = {
        'title': 'About'
    }
    return render(request, 'ipl/about.html', context)


# analysis one data
@cache_page(CACHE_TTL)
def api_analysis_one(request):
    matches_season =\
        Matches.objects.values('season').order_by('season')\
        .annotate(number_of_matches=Count('season'))

    return JsonResponse([match for match in matches_season], safe=False)


# analysis two data
@cache_page(CACHE_TTL)
def api_analysis_two(request):
    numbers_of_wins =\
        Matches.objects.exclude(result='no result').values('winner', 'season')\
        .annotate(number_wins=Count('winner')).order_by('winner', 'season')
    return JsonResponse([win for win in numbers_of_wins], safe=False)


# analysis three data
@cache_page(CACHE_TTL)
def api_analysis_three(request):
    extra_runs_bowling_team =\
        Deliveries.objects.values('bowling_team')\
        .filter(match_id_id__season=2016)\
        .annotate(sum_extra_runs=Sum('extra_runs'))
    return JsonResponse(
        [team_extra_runs for team_extra_runs in extra_runs_bowling_team],
        safe=False
    )


# analysis four data
@cache_page(CACHE_TTL)
def api_analysis_four(request):
    bowlers_economical =\
        Deliveries.objects.values('bowler').filter(match_id_id__season=2015)\
        .annotate(
            economical=Cast(
                Sum('total_runs')/(Count('overs')/6.0), FloatField())
        ).order_by('economical')
    return JsonResponse(
        [bowler_economical for bowler_economical in bowlers_economical[:5]],
        safe=False
    )


# analysis five data
@cache_page(CACHE_TTL)
def api_analysis_five(request):
    win_tosses_total =\
        Matches.objects.filter(result='normal')\
        .aggregate(
            bat=Count('pk', filter=(
                (Q(winner=F('toss_winner')) & Q(toss_decision='bat')) |
                (Q(toss_decision='field') & ~Q(winner=F('toss_winner'))))),
            field=Count('pk', filter=(
                (Q(winner=F('toss_winner')) & Q(toss_decision='field')) |
                (Q(toss_decision='bat') & ~Q(winner=F('toss_winner'))))))
    return JsonResponse(win_tosses_total, safe=False)


# analysis one view
def analysis_one(request):
    context = {
        'title': 'Analysis One'
    }
    return render(request, 'ipl/analysis_one.html', context)


# analysis two view
def analysis_two(request):
    context = {
        'title': 'Analysis Two'
    }
    return render(request, 'ipl/analysis_two.html', context)


# analysis three view
def analysis_three(request):
    context = {
        'title': 'Analysis Three'
    }
    return render(request, 'ipl/analysis_three.html', context)


# analysis four view
def analysis_four(request):
    context = {
        'title': 'Analysis Four'
    }
    return render(request, 'ipl/analysis_four.html', context)


# analysis five view
def analysis_five(request):
    context = {
        'title': 'Analysis Five'
    }
    return render(request, 'ipl/analysis_five.html', context)
